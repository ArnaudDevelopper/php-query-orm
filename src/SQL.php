<?php

namespace QueryBuilder;

class SQLKeyword {
    public static SQLKeyword $IS_NULL;
    public static SQLKeyword $IS_NOT_NULL;
    public static SQLKeyword $IN;
    public static SQLKeyword $NOT_IN;
    public static SQLKeyword $AND;
    public static SQLKeyword $OR;
    public static SQLKeyword $EQ;
    public static SQLKeyword $NEQ;
    public static SQLKeyword $GT;
    public static SQLKeyword $LT;
    public static SQLKeyword $GEQ;
    public static SQLKeyword $LEQ;
    
    private string $value = "";
    
    private function __construct(string $__value)
    {
        $this->value = $__value;
    }

    function __toString()
    {
        return $this->value;
    }

    static public function IS_NULL() {
        if (!isset($IS_NULL))
            $IS_NULL = new SQLKeyword("is null");
        return $IS_NULL;
    }

    static public function IS_NOT_NULL() {
        if (!isset($IS_NOT_NULL))
            $IS_NOT_NULL = new SQLKeyword("is not null");
        return $IS_NOT_NULL;
    }

    static public function IN() {
        if (!isset($IN))
            $IN = new SQLKeyword("in");
        return $IN;
    }

    static public function NOT_IN() {
        if (!isset($NOT_IN))
            $NOT_IN = new SQLKeyword("not in");
        return $NOT_IN;
    }

    static public function AND() {
        if (!isset($AND))
            $AND = new SQLKeyword("and");
        return $AND;
    }

    static public function OR() {
        if (!isset($OR))
            $OR = new SQLKeyword("or");
        return $OR;
    }

        static public function EQ() {
        if (!isset($EQ))
            $EQ = new SQLKeyword("=");
        return $EQ;
    }

    static public function NEQ() {
        if (!isset($NEQ))
            $NEQ = new SQLKeyword("!=");
        return $NEQ;
    }

    static public function GT() {
        if (!isset($GT))
            $GT = new SQLKeyword(">");
        return $GT;
    }

    static public function LT() {
        if (!isset($LT))
            $LT = new SQLKeyword("<");
        return $LT;
    }

    static public function GEQ() {
        if (!isset($GEQ))
            $GEQ = new SQLKeyword(">=");
        return $GEQ;
    }

    static public function LEQ() {
        if (!isset($LEQ))
            $LEQ = new SQLKeyword("<=");
        return $LEQ;
    }
}

class SQL {
    private static $isInitialised = false;
    public static SQLKeyword $IS_NULL;
    public static SQLKeyword $IS_NOT_NULL;
    public static SQLKeyword $IN;
    public static SQLKeyword $NOT_IN;
    public static SQLKeyword $AND;
    public static SQLKeyword $OR;
    public static SQLKeyword $EQ;
    public static SQLKeyword $NEQ;
    public static SQLKeyword $GT;
    public static SQLKeyword $LT;
    public static SQLKeyword $GEQ;
    public static SQLKeyword $LEQ;

    static public function init() {
        if (!self::$isInitialised) {
            self::$IS_NULL = SQLKeyword::IS_NULL();
            self::$IS_NOT_NULL = SQLKeyword::IS_NOT_NULL();
            self::$IN = SQLKeyword::IN();
            self::$NOT_IN = SQLKeyword::NOT_IN();
            self::$AND = SQLKeyword::AND();
            self::$OR = SQLKeyword::OR();
            self::$EQ = SQLKeyword::EQ();
            self::$NEQ = SQLKeyword::NEQ();
            self::$GT = SQLKeyword::GT();
            self::$LT = SQLKeyword::LT();
            self::$GEQ = SQLKeyword::GEQ();
            self::$LEQ = SQLKeyword::LEQ();
        }
        self::$isInitialised = true;
    }

    static private function sqlFunction($function, ...$params) {
        return new Prop("$function(".join(", ", $params).")");
    }

    static private function leftRightOperator($operator, $attr1, $attr2) {
        return new Prop("$attr1$operator$attr2");
    }

    static public function SUM($attr) {
        return self::sqlFunction("SUM", $attr);
    }

    static public function COUNT($attr) {
        return self::sqlFunction("COUNT", $attr);
    }

    static public function MAX($attr) {
        return self::sqlFunction("MAX", $attr);
    }

    static public function MIN($attr) {
        return self::sqlFunction("MIN", $attr);
    }

    static public function AVG($attr) {
        return self::sqlFunction("AVG", $attr);
    }

    static public function DAY($attr) {
        return self::sqlFunction("DAY", $attr);
    }

    static public function MONTH($attr) {
        return self::sqlFunction("MONTH", $attr);
    }

    static public function YEAR($attr) {
        return self::sqlFunction("YEAR", $attr);
    }

    static public function PLUS($attr1, $attr2) {
        return self::leftRightOperator("+", $attr1, $attr2);
    }

    static public function MINUS($attr1, $attr2) {
        return self::leftRightOperator("-", $attr1, $attr2);
    }

    static public function MULT($attr1, $attr2) {
        return self::leftRightOperator("*", $attr1, $attr2);
    }

    static public function DIV($attr1, $attr2) {
        return self::leftRightOperator("/", $attr1, $attr2);
    }

    static public function TIMESTAMPDIFF($unit, $attr1, $attr2) {
        return self::sqlFunction("TIMESTAMPDIFF", $unit, $attr1, $attr2);
    }
}
SQL::init();