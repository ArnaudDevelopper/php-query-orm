<?php

namespace QueryBuilder;

use QueryBuilder\Utils;
use QueryBuilder\Prop;

require_once 'vendor/autoload.php';



class Model
{
    /**
     * @brief CRUD Operation to GET
     * By default, if the first parameter of select method is a child of Query,
     * the behavior will be to return an instance of queryBuilder with the requestString : "SELECT * FROM CHILDTABLE"
     */
    public static function select(...$args): Query
    {
        return Query::select(get_called_class(), ...$args);
    }

    /**
     * @brief CRUD Operation to CREATE
     */
    public static function create(...$arrayData)
    {
        return Query::create(get_called_class(), $arrayData);
    }

    /**
     * @brief CRUD Operation to UPDATE
     */
    public static function update(...$arrayData)
    {
        return Query::update(get_called_class(), $arrayData);
    }

    /**
     * @brief CRUD Operation to DELETE
     */
    public static function delete()
    {
        return Query::delete(get_called_class());
    }

    public static function mapClass()
    {
        $reflectionClass = new \ReflectionClass(get_called_class());

        $attrStatics = $reflectionClass->getProperties(\ReflectionProperty::IS_STATIC);

        foreach ($attrStatics as $i => $value) {
            if($value->getType()->getName() == Prop::class)
                $reflectionClass->setStaticPropertyValue($value->getName(), new Prop(Utils::parseClassPath(get_called_class()) . "." . $value->getName()));
        }
    }

    /**
     * @brief Alias for (SELECT * FROM model)->computeString() 
     */
    public static function computeString(): string
    {
        return self::select()->computeString();
    }

    /**
     * @brief Alias for (SELECT * FROM model)->where($args) 
     */
    public static function where($args)
    {
        return self::select()->where($args);
    }

    /**
     * @brief Alias for (SELECT * FROM model)->groupBy($args) 
     */
    public static function groupBy(...$args)
    {
        return self::select()->groupBy(...$args);
    }

    /**
     * @brief Alias for (SELECT * FROM model)->having($args) 
     */
    public static function having($args)
    {
        return self::select()->having($args);
    }

    /**
     * @brief Alias for (SELECT * FROM model)->join($args) 
     */
    public static function join(...$args)
    {
        return self::select()->join(...$args);
    }

    /**
     * @brief Alias for (SELECT * FROM model)->execute($args) 
     */
    public static function execute()
    {
        return self::select()->execute();
    }
}
