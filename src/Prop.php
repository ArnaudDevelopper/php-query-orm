<?php

namespace QueryBuilder;

use QueryBuilder\Utils;


require_once 'vendor/autoload.php';

class Prop
{
    private string $attributeName;

    function __construct(string $__attributeName)
    {
        $this->attributeName = $__attributeName;
    }

    public function __toString()
    {
        return $this->attributeName;
    }

    public function as($nickname) {
        return $this->attributeName." as ".$nickname;
    }
}
