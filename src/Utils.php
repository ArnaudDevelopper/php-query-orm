<?php
namespace QueryBuilder;

class Utils {


    public static function getUpperClass($class) {
        return strtoupper(get_class($class));
    }

    public static function parseClassPath($classPath) {
        $explodedClassName = explode('\\' , $classPath);
        return end($explodedClassName);

    }

}