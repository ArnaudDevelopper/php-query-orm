<?php

namespace QueryBuilder;

use QueryBuilder\Utils;
use QueryBuilder\Prop;

class Query
{
    protected $fromTable = "";
    protected $requestString = "";
    private static $_instanceDb;

    public function __toString()
    {
        return $this->computeString();
    }

    public static function setDatabaseInstance($dbInstance)
    {
        self::$_instanceDb = $dbInstance;
    }


    public static function select(string $modelClassName, ...$args): Query
    {
        $nbrArgs = count($args);

        $query = new Query();
        $query->requestString .= "select";

        if ($nbrArgs === 0) {
            $query->requestString .= " *";
        } else {
            for ($i = 0; $i < $nbrArgs - 1; $i++) {
                $query->requestString .= " " . Utils::parseClassPath($args[$i]) . ",";
            }
            $query->requestString .= " " . Utils::parseClassPath($args[$nbrArgs - 1]);
        }
        $query->fromTable = Utils::parseClassPath($modelClassName);
        $query->requestString .= " from " . $query->fromTable;
        return $query;
    }

    /**
     * @brief CRUD Operation to CREATE
     */
    public static function create(string $modelClassName, $arrayData = [])
    {

        $query = new Query();
        $query->fromTable = Utils::parseClassPath($modelClassName);


        if (count($arrayData) > 0) {

            $stringCreateQuery = "insert into " . $query->fromTable . " set ";
            foreach ($arrayData as $key => $value) {
                $stringCreateQuery .= $value[0] . "='" . $value[1] . "', ";
            }
            $query->requestString .= substr($stringCreateQuery, 0, -2); // Delete the last ,

        } else { // Case where every attribute of the table has a default value.

            $query->requestString = "insert into " . $query->fromTable;
        }

        return $query;
    }

    /**
     * @brief CRUD Operation to UPDATE
     */
    public static function update(string $modelClassName, $arrayData = [])
    {
        $query = new Query();
        $query->fromTable = Utils::parseClassPath($modelClassName);


        if (count($arrayData) > 0) {

            $stringCreateQuery = "update " . $query->fromTable . " set ";
            foreach ($arrayData as $key => $value) {
                $stringCreateQuery .= $value[0] . "='" . $value[1] . "', ";
            }
            $query->requestString .= substr($stringCreateQuery, 0, -2); // Delete the last ,

        } else { // Case where every attribute of the table has a default value.

            $query->requestString = "update " . $query->fromTable;
        }

        return $query;
    }

    /**
     * @brief CRUD Operation to DELETE
     */
    public static function delete(string $modelClassName)
    {
        $query = new Query();
        $query->fromTable = Utils::parseClassPath($modelClassName);
        $query->requestString = "delete from " . $query->fromTable;

        return $query;
    }

    /**
     * @brief Compute the string value of the request
     */
    public function computeString(): string
    {
        return $this->requestString;
    }

    /**
     *  @brief Filtering the request
     *  @example ->where([[$attr, ">", 2], SQL::AND, [$attr2, 4]]) // Which give : "($attr > 2) and ($attr2 = 4)
     */
    public function where($arrayData = [])
    {
        $this->requestString .= " where " . self::formatCondition($arrayData);

        return $this;
    }

    /**
     * @brief Format into string the given condition (used in where method)
     * @example formatCondition([ModelTest1::$attr, SQL::$EQ, 8])
     */
    private static function formatCondition($condition)
    {
        if (gettype($condition) != "array") {
            return "" . self::formatCondValue($condition);
        }
        $attr1 = $condition[0];
        $attr2 = $condition[count($condition) - 1];
        $operator = "";
        switch (count($condition)) {
            case 3:
                $operator = " " . $condition[1] . " ";
                break;
            case 2:
                if (gettype($condition[1]) == "object" && get_class($condition[1]) == SQLKeyword::class) {
                    $operator = " ";
                } else {
                    $operator = " = ";
                }
                break;
            default:
                throw new \Exception("Error while formatting a condition", 1);
                break;
        }
        return "(" . self::formatCondition($attr1) . $operator . self::formatCondition($attr2) . ")";
    }

    private static function formatCondValue($attribute)
    {
        $typeStr = gettype($attribute);
        switch ($typeStr) {
            case "integer":
                return $attribute;
                break;
            case "string":
                return "'" . $attribute . "'";
                break;
            case "object":
                $classStr = get_class($attribute);
                switch ($classStr) {
                    case Query::class:
                        return "(" . $attribute->__toString() . ")";
                        break;
                    case Prop::class:
                    case SQLKeyword::class:
                        return $attribute->__toString();
                        break;
                    default:
                        throw new \Exception("Error while formatting condition value: Invalid arg class: " . $classStr . ", expected : " . SQLKeyword::class . " or " . Prop::class, 1);
                        break;
                }
                break;
            default:
                throw new \Exception("Error while formatting condition value: Unknown type : " . $typeStr, 1);
                break;
        }
    }

    /**
     * @brief Filtering with Group By
     * @example ->groupBy(Attr1, Attr2)
     */
    public function groupBy(...$args)
    {
        $this->requestString .= " group by " . join(", ", $args);

        return $this;
    }

    /**
     * @brief Filtering with Having
     * @example ->having(SUM(Attr1), ">", 4)
     */
    public function having($arrayData = [])
    {
        $this->requestString .= " having " . self::formatCondition($arrayData);

        return $this;
    }

    /**
     * @brief Order by the given attribute
     */
    public function orderBy($attr, $option = "ASC")
    {
        $this->requestString .= " order by " . $attr . " " .$option;

        return $this;
    }

    /**
     * @brief Join 2 tables
     * 
     */
    public function join($parent, $attr1, $attr2, $option = "inner")
    {
        $className = Utils::parseClassPath($parent);


        $this->requestString .= " " . $option . " join " . $className . " on " . $attr1 . "=" . $attr2;

        return $this;
    }

    /**
     * @brief Execute the query from the queryString
     *
     */

    public function execute()
    {
        return self::$_instanceDb->query($this->requestString)->fetchAll();
    }

    /**
     * @brief sql except keyword, need to be used on a Query with a Query as parameter
     */
    public function except($query)
    {
        $this->requestString .= " except " . $query->computeString();

        return $this;
    }

    /**
     * @brief sql intersect keyword, need to be used on a Query with a Query as parameter
     */
    public function intersect($query)
    {
        $this->requestString .= " intersect " . $query->computeString();

        return $this;
    }

    /**
     * @brief sql union keyword, need to be used on a Query with a Query as parameter
     */
    public function union($query)
    {
        $this->requestString .= " union " . $query->computeString();

        return $this;
    }
}
