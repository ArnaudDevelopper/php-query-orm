<?php

declare(strict_types=1);

namespace tst\PropTest;

require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertEquals;
use QueryBuilder\Model;
use QueryBuilder\Prop;

final class ModelTest1 extends Model
{
    static Prop $Attr1;
    static Prop $Attr2;
    static Prop $Attr3;
    static Prop $Attr4;
}

ModelTest1::mapClass();

final class PropTest extends TestCase {

    public function testAs() {
        assertEquals(ModelTest1::$Attr1->as("test"), "ModelTest1.Attr1 as test");
        assertEquals(ModelTest1::$Attr1, "ModelTest1.Attr1");
        assertEquals(ModelTest1::$Attr1->as("test2"), "ModelTest1.Attr1 as test2");
        assertEquals(ModelTest1::$Attr1, "ModelTest1.Attr1");
    }

}