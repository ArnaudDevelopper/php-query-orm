<?php
declare(strict_types=1);
namespace tst\ModelTest;
require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertEquals;

use QueryBuilder\Model;
use QueryBuilder\Prop;

final class ModelTest1 extends Model
{
    static Prop $StaticAttribute1;
    static Prop $StaticAttribute2;
    static Prop $StaticAttribute3;
}
ModelTest1::mapClass();

final class ModelTest extends TestCase
{
    public function testMapClass()
    {
        assertEquals("ModelTest1.StaticAttribute1", ModelTest1::$StaticAttribute1);
        assertEquals("ModelTest1.StaticAttribute2", ModelTest1::$StaticAttribute2);
        assertEquals("ModelTest1.StaticAttribute3", ModelTest1::$StaticAttribute3);
    }
}
