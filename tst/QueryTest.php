<?php

declare(strict_types=1);

namespace tst\QueryTest;

require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertEquals;

use QueryBuilder\Model as Model;
use QueryBuilder\SQL;
use QueryBuilder\Prop;
use tst\ModelTest\ModelTest;

final class ModelTest1 extends Model
{
    static Prop $Attr1;
    static Prop $Attr2;
    static Prop $Attr3;
    static Prop $Attr4;
}
ModelTest1::mapClass();

final class ModelTest2 extends Model
{
    static Prop $Attr1;
    static Prop $Attr2;
    static Prop $Attr3;
    static Prop $Attr4;
}
ModelTest2::mapClass();

final class QueryTest extends TestCase
{
    public function testSelectFields()
    {
        assertEquals(
            "select ModelTest1.Attr1 from ModelTest1",
            ModelTest1::select(ModelTest1::$Attr1)->computeString()
        );
        assertEquals(
            "select ModelTest1.Attr3 from ModelTest1",
            ModelTest1::select(ModelTest1::$Attr3)->computeString()
        );
        assertEquals(
            "select ModelTest1.Attr1, ModelTest1.Attr4 from ModelTest1",
            ModelTest1::select(ModelTest1::$Attr1, ModelTest1::$Attr4)->computeString()
        );
        assertEquals(
            "select ModelTest1.Attr4, ModelTest1.Attr1, ModelTest1.Attr3 from ModelTest1",
            ModelTest1::select(ModelTest1::$Attr4, ModelTest1::$Attr1, ModelTest1::$Attr3)->computeString()
        );
    }

    public function testCreateFields()
    {
        assertEquals(
            "insert into ModelTest1",
            ModelTest1::create()->computeString()
        );
        assertEquals(
            "insert into ModelTest1 set ModelTest1.Attr1='test'",
            ModelTest1::create([ModelTest1::$Attr1, "test"])->computeString()
        );
        assertEquals(
            "insert into ModelTest1 set ModelTest1.Attr1='test', ModelTest1.Attr2='test'",
            ModelTest1::create([ModelTest1::$Attr1, "test"], [ModelTest1::$Attr2, "test"])->computeString()
        );
    }

    public function testDelete()
    {
        assertEquals(
            "delete from ModelTest1",
            ModelTest1::delete()->computeString()
        );
    }

    public function testUpdateFields()
    {
        assertEquals(
            "update ModelTest1 set ModelTest1.Attr1='test'",
            ModelTest1::update([ModelTest1::$Attr1, "test"])->computeString()
        );
        assertEquals(
            "update ModelTest1 set ModelTest1.Attr1='test', ModelTest1.Attr2='test'",
            ModelTest1::update([ModelTest1::$Attr1, "test"], [ModelTest1::$Attr2, "test"])->computeString()
        );
    }

    public function testSelectWhere()
    {
        assertEquals(
            "select * from ModelTest1 where (ModelTest1.Attr1 = 'test')",
            ModelTest1::where([ModelTest1::$Attr1, "test"])->computeString()
        );
        assertEquals(
            "select * from ModelTest1 where ((ModelTest1.Attr1 = 'test') and (ModelTest1.Attr2 = 'test'))",
            ModelTest1::where([[ModelTest1::$Attr1, "test"], SQL::$AND, [ModelTest1::$Attr2, "test"]])->computeString()
        );
        assertEquals(
            "select * from ModelTest1 where (ModelTest1.Attr2 is null)",
            ModelTest1::where([ModelTest1::$Attr2, SQL::$IS_NULL])->computeString()
        );
        assertEquals(
            "select * from ModelTest1 where (ModelTest1.Attr2 is not null)",
            ModelTest1::where([ModelTest1::$Attr2, SQL::$IS_NOT_NULL])->computeString()
        );
        assertEquals(
            "select * from ModelTest1 where ((ModelTest1.Attr1 > ModelTest1.Attr2) and ((ModelTest1.Attr3 is null) or ('test' = ModelTest1.Attr3)))",
            ModelTest1::where([[ModelTest1::$Attr1, SQL::$GT, ModelTest1::$Attr2], SQL::$AND, [[ModelTest1::$Attr3, SQL::$IS_NULL], SQL::$OR, ["test", SQL::$EQ, ModelTest1::$Attr3]]])->computeString()
        );
    }

    public function testSelectJoin()
    {
        assertEquals(
            "select * from ModelTest1 inner join ModelTest2 on ModelTest1.Attr1=ModelTest2.Attr1",
            ModelTest1::join(ModelTest2::class, ModelTest1::$Attr1, ModelTest2::$Attr1)->computeString()
        );
        assertEquals(
            "select ModelTest1.Attr2, ModelTest2.Attr3, ModelTest1.Attr1 from ModelTest1 inner join ModelTest2 on ModelTest1.Attr4=ModelTest2.Attr4",
            ModelTest1::select(ModelTest1::$Attr2, ModelTest2::$Attr3, ModelTest1::$Attr1)
                ->join(ModelTest2::class, ModelTest1::$Attr4, ModelTest2::$Attr4)
                ->computeString()
        );
        assertEquals(
            "select ModelTest1.Attr2, ModelTest2.Attr3, ModelTest1.Attr1 from ModelTest1 inner join ModelTest2 on ModelTest2.Attr4=ModelTest1.Attr3",
            ModelTest1::select(ModelTest1::$Attr2, ModelTest2::$Attr3, ModelTest1::$Attr1)
                ->join(ModelTest2::class, ModelTest2::$Attr4, ModelTest1::$Attr3)
                ->computeString()
        );
        assertEquals(
            "select ModelTest1.Attr2, ModelTest2.Attr3, ModelTest1.Attr1 from ModelTest1 inner join ModelTest2 on ModelTest2.Attr4=ModelTest1.Attr3 inner join ModelTest1 on ModelTest1.Attr3=ModelTest2.Attr2",
            ModelTest1::select(ModelTest1::$Attr2, ModelTest2::$Attr3, ModelTest1::$Attr1)
                ->join(ModelTest2::class, ModelTest2::$Attr4, ModelTest1::$Attr3)
                ->join(ModelTest1::class, ModelTest1::$Attr3, ModelTest2::$Attr2)
                ->computeString()
        );
    }

    public function testGroupBy()
    {
        assertEquals(
            "select * from ModelTest1 group by ModelTest1.Attr2",
            ModelTest1::groupBy(ModelTest1::$Attr2)->computeString()
        );
        assertEquals(
            "select * from ModelTest1 group by ModelTest1.Attr2, ModelTest1.Attr4",
            ModelTest1::groupBy(ModelTest1::$Attr2, ModelTest1::$Attr4)->computeString()
        );
    }

    public function testSelectHaving()
    {
        assertEquals(
            "select * from ModelTest1 having (SUM(ModelTest1.Attr1) > 2)",
            ModelTest1::having([SQL::SUM(ModelTest1::$Attr1), SQL::$GT, 2])->computeString()
        );
        assertEquals(
            "select ModelTest1.Attr3, SUM(ModelTest1.Attr2) from ModelTest1 group by ModelTest1.Attr3 having (SUM(ModelTest1.Attr2) <= -5)",
            ModelTest1::select(ModelTest1::$Attr3, SQL::SUM(ModelTest1::$Attr2))
                ->groupBy(ModelTest1::$Attr3)
                ->having([SQL::SUM(ModelTest1::$Attr2), SQL::$LEQ, -5])
                ->computeString()
        );
        assertEquals(
            "select ModelTest1.Attr3, SUM(ModelTest1.Attr2) from ModelTest1 group by ModelTest1.Attr3 having ((SUM(ModelTest1.Attr2) <= -5) and (COUNT(*) is not null))",
            ModelTest1::select(ModelTest1::$Attr3, SQL::SUM(ModelTest1::$Attr2))
                ->groupBy(ModelTest1::$Attr3)
                ->having([[SQL::SUM(ModelTest1::$Attr2), SQL::$LEQ, -5], SQL::$AND, [SQL::COUNT("*"), SQL::$IS_NOT_NULL]])
                ->computeString()
        );
    }

    public function testExcept()
    {
        assertEquals(
            "select * from ModelTest1 having (SUM(ModelTest1.Attr1) > 2) except select ModelTest2.Attr3 from ModelTest2",
            ModelTest1::having([SQL::SUM(ModelTest1::$Attr1), ">", 2])
                ->except(
                    ModelTest2::select(ModelTest2::$Attr3)
                )->computeString()
        );
    }

    public function testIntersect()
    {
        assertEquals(
            "select * from ModelTest1 having (SUM(ModelTest1.Attr1) > 2) intersect select ModelTest2.Attr3 from ModelTest2",
            ModelTest1::having([SQL::SUM(ModelTest1::$Attr1), ">", 2])
                ->intersect(
                    ModelTest2::select(ModelTest2::$Attr3)
                )->computeString()
        );
    }

    public function testUnion()
    {
        assertEquals(
            "select * from ModelTest1 having (SUM(ModelTest1.Attr1) > 2) union select ModelTest2.Attr3 from ModelTest2",
            ModelTest1::having([SQL::SUM(ModelTest1::$Attr1), ">", 2])
                ->union(
                    ModelTest2::select(ModelTest2::$Attr3)
                )->computeString()
        );
    }

    public function testComplexRequest()
    {
        assertEquals(
            "select ModelTest1.Attr1 from ModelTest1 except select ModelTest1.Attr2 from ModelTest1 inner join ModelTest2 on ModelTest2.Attr4=ModelTest1.Attr4 where (ModelTest2.Attr3 not in (select ModelTest2.Attr1 from ModelTest2 inner join ModelTest1 on ModelTest1.Attr1=ModelTest2.Attr3 where (ModelTest2.Attr4 is not null))) group by ModelTest2.Attr1",
            ModelTest1::select(ModelTest1::$Attr1)
                ->except(
                    ModelTest1::select(ModelTest1::$Attr2)
                        ->join(ModelTest2::class, ModelTest2::$Attr4, ModelTest1::$Attr4)
                        ->where([
                            ModelTest2::$Attr3, SQL::$NOT_IN,
                            ModelTest2::select(ModelTest2::$Attr1)
                                ->join(ModelTest1::class, ModelTest1::$Attr1, ModelTest2::$Attr3)
                                ->where([ModelTest2::$Attr4, SQL::$IS_NOT_NULL])
                        ])
                        ->groupBy(ModelTest2::$Attr1)
                )->computeString()
        );
    }

    public function testOrderBy() {
        assertEquals(
            "select * from ModelTest1 order by ModelTest1.Attr2 ASC",
            ModelTest1::select()->orderBy(ModelTest1::$Attr2)->computeString()
        );
        assertEquals(
            "select * from ModelTest1 order by ModelTest1.Attr2 ASC",
            ModelTest1::select()->orderBy(ModelTest1::$Attr2, "ASC")->computeString()
        );
        assertEquals(
            "select * from ModelTest1 order by ModelTest1.Attr2 DESC",
            ModelTest1::select()->orderBy(ModelTest1::$Attr2, "DESC")->computeString()
        );
    }
}
