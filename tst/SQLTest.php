<?php

declare(strict_types=1);

namespace tst\ModelTest;

require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertEquals;

use QueryBuilder\SQL;

final class SQLTest extends TestCase {
    public function testUnaryFunctions() {
        assertEquals("SUM(attribute)", SQL::SUM("attribute")->__toString());
        assertEquals("AVG(attribute)", SQL::AVG("attribute")->__toString());
        assertEquals("COUNT(attribute)", SQL::COUNT("attribute")->__toString());
        assertEquals("MIN(attribute)", SQL::MIN("attribute")->__toString());
        assertEquals("MAX(attribute)", SQL::MAX("attribute")->__toString());
        assertEquals("DAY(attribute)", SQL::DAY("attribute")->__toString());
        assertEquals("MONTH(attribute)", SQL::MONTH("attribute")->__toString());
        assertEquals("YEAR(attribute)", SQL::YEAR("attribute")->__toString());
    }

    public function testBinaryOperators() {
        assertEquals("attribute1+attribute2", SQL::PLUS("attribute1", "attribute2")->__toString());
        assertEquals("attribute1-attribute2", SQL::MINUS("attribute1", "attribute2")->__toString());
        assertEquals("attribute1*attribute2", SQL::MULT("attribute1", "attribute2")->__toString());
        assertEquals("attribute1/attribute2", SQL::DIV("attribute1", "attribute2")->__toString());
    }

    public function testVariousFunctions() {
        assertEquals("TIMESTAMPDIFF(MINUTE, attribute1, attribute2)", SQL::TIMESTAMPDIFF("MINUTE", "attribute1", "attribute2")->__toString());
    }
}
